<?php
/** @var array $result */
?>
    <h1><?=$result['title']?></h1>
<?php foreach ($result['opportunities'] as $item): ?>
    <button
        onclick="window.location.href='<?=$item['link']?>';"><?=$item['name']?></button>
    <br>
    <br>
<?php endforeach; ?>
<?php


namespace Model;


use App\Helper\Generator;
use App\Helper\ModelEntity;
use Service\RecoveryPassword;

class User extends ModelEntity
{
    public $id;
    public /** @noinspection PhpUnused */
           $name;
    
    public $email;
    public $role;
    public $password;
    public $passwordHash;
    public $recoveryToken;
    public /** @noinspection PhpUnused */
           $isBanned;
    
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->role     = 1;
        $this->isBanned = false;
    }
    
    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'user';
    }
    /** @noinspection PhpUnused */
    
    /**
     * @return bool
     */
    public function recoveryPassword()
    {
        if ($recoveryToken = RecoveryPassword::addUser($this->email)) {
            $this->recoveryToken = $recoveryToken;
            return $this->update();
        }
        return false;
    }
    
    /**
     * @return bool
     */
    public function add()
    {
        $this->passwordHash();
        return parent::update();
    }
    
    /**
     * @return bool
     */
    public function update()
    {
        $this->passwordHash();
        return parent::update();
    }
    
    /** @noinspection ClassMethodNameMatchesFieldNameInspection */
    /**
     * @return void
     */
    private function passwordHash()
    {
        if ($this->password) {
            $this->passwordHash = Generator::generatePasswordHash($this->password);
        }
    }
}
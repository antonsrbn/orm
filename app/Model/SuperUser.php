<?php

namespace Model;


class SuperUser extends User
{
    /** @noinspection PhpUnused */
    
    /**
     * @param \Model\User $user
     */
    public static function banUser(User $user)
    {
        $user->isBanned = true;
        $user->update();
    }
}
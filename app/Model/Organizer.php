<?php

namespace Model;

class Organizer extends SuperUser
{
    /**
     * Organizer constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->role = 2;
    }
    
    /** @noinspection PhpUnused */
    
    /**
     * @param string $eventName
     * @return string
     */
    public function createEvent($eventName = 'undefined')
    {
        $model       = new Event();
        $organizerId = $this->id;
        $model->fill([
            'eventName'   => $eventName,
            'organizerId' => $organizerId,
        ]);
        return $model->add();
    }
    
    /** @noinspection PhpUnused */
    
    /**
     * @param \Model\User  $user
     * @param \Model\Event $event
     * @return string
     */
    public function toInviteUser(User $user, Event $event)
    {
        $model = new UserToEvent();
        $model->fill([
            'userId'  => $user->id,
            'eventId' => $event->id,
        ]);
        return $model->add();
    }
}
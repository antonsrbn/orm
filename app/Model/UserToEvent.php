<?php


namespace Model;


use App\Helper\ModelEntity;

class UserToEvent extends ModelEntity
{
    public /** @noinspection PhpUnused */
        $eventId;
    public /** @noinspection PhpUnused */
        $userId;
}
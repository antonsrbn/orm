<?php

namespace Model;

class Admin extends SuperUser
{
    /**
     * Admin constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->role = 3;
    }
    
    /** @noinspection PhpUnused */
    
    
    /**
     * @param \Model\User $user
     * @return int
     */
    public static function addUser(User $user)
    {
        return $user->add();
    }
    
    /** @noinspection PhpUnused */
    
    
    /**
     * @param \Model\User $user
     */
    public static function deleteUser(User $user)
    {
        $user->delete();
    }
    
    /** @noinspection PhpUnused */
    
    
    /**
     * @param \Model\Organizer $organizer
     * @return string
     */
    public static function addOrganizer(Organizer $organizer)
    {
        return $organizer->add();
    }
    
    /** @noinspection PhpUnused */
    
    
    /**
     * @param \Model\Organizer $organizer
     */
    public static function deleteOrganizer(Organizer $organizer)
    {
        $organizer->delete();
    }
}
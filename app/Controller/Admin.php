<?php


namespace Controller;


use App\Helper\Template;
use Model\Admin as AdminEntity;
use Model\User;
use Model\Organizer;

/** @noinspection PhpUnused */

class Admin
{
    /** @noinspection PhpUnused */
    
    /**
     * @return \App\Helper\Template
     */
    public function index()
    {
        $opportunities = [
            [
                'name' => 'Добавить пользователя',
                'link' => '/admin/addUser',
            ],
            [
                'name' => 'Удалить пользователя',
                'link' => '/admin/deleteUser',
            ],
            [
                'name' => 'Добавить организатора',
                'link' => '/admin/addOrganizer',
            ],
            [
                'name' => 'Удалить организатора',
                'link' => '/admin/deleteOrganizer',
            ],
            [
                'name' => 'Забанить пользователя',
                'link' => '/admin/banUser',
            ],
        ];
        $data          = [
            'title'         => 'Администратор',
            'opportunities' => $opportunities,
        ];
        return new Template('opportunities', $data);
    }
    
    /** @noinspection PhpUnused */
    public function addUser()
    {
        $properties = [
            'id'       => '2',
            'email'    => '2testemail2',
            'name'     => '2testname2',
            'password' => '2testpassword2',
        ];
        $user       = new User;
        $user->fill($properties);
        AdminEntity::addUser($user);
    }
    
    /** @noinspection PhpUnused */
    public function deleteUser()
    {
        $id   = 1;
        $user = User::query()->findOne($id);
        AdminEntity::deleteUser($user);
    }
    
    /** @noinspection PhpUnused */
    public function addOrganizer()
    {
        $properties = [
            'id'       => '3',
            'email'    => '3testemail3',
            'name'     => '3testname3',
            'password' => '3testpassword3',
        ];
        $organizer  = new Organizer();
        $organizer->fill($properties);
        AdminEntity::addOrganizer($organizer);
    }
    
    /** @noinspection PhpUnused */
    public function deleteOrganizer()
    {
        $id        = 1;
        $organizer = Organizer::query()->findOne($id);
        AdminEntity::deleteOrganizer($organizer);
    }
    
    /** @noinspection PhpUnused */
    public function banUser()
    {
        $id   = 1;
        $user = User::query()->findOne($id);
        AdminEntity::banUser($user);
    }
}
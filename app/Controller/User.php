<?php


namespace Controller;


use App\Helper\Debugger;
use App\Helper\Template;
use Model\User as UserEntity;

/** @noinspection PhpUnused */

class User
{
    /** @noinspection PhpUnused */
    /**
     * @return \App\Helper\Template
     */
    public function index()
    {
        $opportunities = [
            [
                'name' => 'Получить персональные данные',
                'link' => '/user/getData',
            ],
            [
                'name' => 'Получить письмо восстановления пароля',
                'link' => '/user/emailRecoveryPassword',
            ],
        ];
        $data          = [
            'title'         => 'Пользователь',
            'opportunities' => $opportunities,
        ];
        return new Template('opportunities', $data);
    }
    
    /** @noinspection PhpUnused */
    public function getData()
    {
        $id     = 1;
        $result = UserEntity::query()->findOne($id);
        Debugger::viewDump($result);
    }
    
    /** @noinspection PhpUnused */
    public function emailRecoveryPassword()
    {
        $id     = 1;
        $result = UserEntity::query()->findOne($id);
        Debugger::viewDump($result->recoveryPassword());
    }
}
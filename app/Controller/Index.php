<?php

namespace Controller;

use App\Helper\Template;

/** @noinspection PhpUnused */

class Index
{
    /** @noinspection PhpUnused */
    
    /**
     * @return \App\Helper\Template
     */
    public function index()
    {
        $data =
            [
                'title'  => 'Стартовая страница',
                'button' => [
                    'title' => 'Роли',
                    'link'  => '/index/role',
                ],
            ];
        return new Template('index', $data);
    }
    
    /** @noinspection PhpUnused */
    
    /**
     * @return \App\Helper\Template
     */
    public function role()
    {
        $opportunities = [
            [
                'name' => 'Пользователь',
                'link' => '/user',
            ],
            [
                'name' => 'Администратор',
                'link' => '/admin',
            ],
            [
                'name' => 'Организатор',
                'link' => '/organizer',
            ],
        ];
        $data          = [
            'title'         => 'Роли',
            'opportunities' => $opportunities,
        ];
        return new Template('opportunities', $data);
    }
}
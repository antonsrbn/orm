<?php


namespace Controller;


use App\Helper\Template;
use Model\Organizer as OrganizerEntity;
use Model\Event;
use Model\User;

/** @noinspection PhpUnused */

class Organizer
{
    /** @noinspection PhpUnused */
    
    /**
     * @return \App\Helper\Template
     */
    public function index()
    {
        $opportunities = [
            [
                'name' => 'Создать мероприятие',
                'link' => '/organizer/createEvent',
            ],
            [
                'name' => 'Пригласить пользователя в мероприятие',
                'link' => '/organizer/toInviteUser',
            ],
            [
                'name' => 'Забанить пользователя',
                'link' => '/organizer/banUser',
            ],
        ];
        $data          = [
            'title'         => 'Организатор',
            'opportunities' => $opportunities,
        ];
        return new Template('opportunities', $data);
    }
    
    
    /** @noinspection PhpUnused */
    public function banUser()
    {
        $id   = 1;
        $user = User::query()->findOne($id);
        OrganizerEntity::banUser($user);
    }
    
    
    /** @noinspection PhpUnused */
    public function createEvent()
    {
        $id        = 1;
        $organizer = OrganizerEntity::query()->findOne($id);
        $organizer->createEvent();
    }
    
    /** @noinspection PhpUnused */
    public function toInviteUser()
    {
        $id        = 1;
        $organizer = OrganizerEntity::query()->findOne($id);
        $eventId   = 2;
        $event     = Event::query()->findOne($eventId);
        $userId    = 3;
        $user      = User::query()->findOne($userId);
        $organizer->toInviteUser($user, $event);
    }
}
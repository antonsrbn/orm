<?php

namespace App;

use App\Helper\Template;

class Application
{
    const DEFAULT_CONTROLLER = 'Index';
    const DEFAULT_METHOD     = 'index';
    
    public function __construct()
    {
        $path = explode('/', $_SERVER['REQUEST_URI']);
        
        $controllerName = '\\Controller\\' . ($path[1] ? ucfirst($path[1]) : self::DEFAULT_CONTROLLER);
        $methodName     = $path[2] ? ucfirst($path[2]) : self::DEFAULT_METHOD;
        $result         = (new $controllerName())->$methodName();
        if($result instanceof Template) {
            echo $result();
        }
    }
}
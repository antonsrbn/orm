<?php


namespace App\Helper;


class Generator
{
    public static function generateToken()
    {
        /** @noinspection CryptographicallySecureRandomnessInspection */
        return bin2hex(openssl_random_pseudo_bytes(64));
    }
    
    /**
     * @param $password
     * @return false|string
     */
    public static function generatePasswordHash($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
    
    /** @noinspection PhpUnused */
    /**
     * @param $password
     * @param $hash
     * @return bool
     */
    public static function passwordVerify($password, $hash)
    {
        return password_verify ($password, $hash);
    }
    
}
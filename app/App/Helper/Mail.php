<?php


namespace App\Helper;


class Mail
{
    protected $to;
    protected $theme;
    protected $message;
    
    
    /**
     * @return bool
     */
    public function send()
    {
        return mail($this->to, $this->theme, $this->message);
    }
    
    /**
     * @param mixed $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }
    
    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }
    
    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}
<?php


namespace App\Helper;


class QueryBilder
{
    protected $tableName;
    protected $className;
    protected $where;
    
    
    /**
     * QueryBilder constructor.
     * @param $tableName
     * @param $className
     */
    public function __construct($tableName, $className)
    {
        $this->tableName = $tableName;
        $this->className = '\\' . $className;
    }
    
    /**
     * @param $id
     * @return mixed
     */
    public function findOne($id)
    {
        $this->where = 'WHERE id = "' . $id . '"';
        /** @var \App\Helper\ModelEntity $object */
        $object = new $this->className;
        /*
         * Здесь происходит запрос и заполнение сущности
         * Тестовые данные:
        */
        $properties = [
            'id'           => '1',
            'email'        => 'testemail',
            'name'         => 'testname',
            'passwordHash' => 'testpasswordhash',
        ];
        $object->fill($properties);
        return $object;
    }
}
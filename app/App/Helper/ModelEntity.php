<?php


namespace App\Helper;


abstract class ModelEntity
{
    protected static $tableName;
    
    /**
     * ModelEntity constructor.
     */
    public function __construct()
    {
        self::$tableName = static::getTableName();
    }
    
    /**
     * @return \App\Helper\QueryBilder
     */
    public static function query()
    {
        self::$tableName = static::getTableName();
        echo 'Выборка из таблицы ' . self::$tableName . '<br>';
        return new QueryBilder(self::$tableName, static::class);
    }
    
    /**
     * @return string
     */
    protected static function getTableName()
    {
        $calledClassFullName = explode('\\', static::class);
        $calledClassName     = array_pop($calledClassFullName);
        return strtolower(
            preg_replace(
                '/([a-z]+)([A-Z]+)/',
                '$1_$2',
                lcfirst(
                    $calledClassName
                )
            )
        );
    }
    
    /**
     * @param $properties
     */
    public function fill($properties)
    {
        foreach ($properties as $key => $value) {
            $this->$key = $value;
        }
    }
    
    /**
     * @return string
     */
    public function add()
    {
        echo 'Добавление в таблицу ' . self::$tableName . ' строку' . '<br>';
        Debugger::viewDump($this);
        $this->id = '9999';
        return $this->id;
    }
    
    /**
     * @return bool
     */
    public function update()
    {
        echo 'Обновление в ' . self::$tableName . ' строки' . '<br>';
        Debugger::viewDump($this);
        return true;
    }
    
    /**
     *
     */
    public function delete()
    {
        echo 'Удаление в ' . self::$tableName . ' строки' . '<br>';
        Debugger::viewDump($this);
    }
}
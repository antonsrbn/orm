<?php


namespace App\Helper;


class Debugger
{
    public static function viewDump($array)
    {
        echo '<pre>';
        var_dump($array);
        echo '</pre>';
    }
}
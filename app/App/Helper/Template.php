<?php


namespace App\Helper;


class Template
{
    protected $pathTemplate;
    protected $result;
    
    /**
     * Template constructor.
     * @param       $templateName
     * @param array $result
     */
    public function __construct($templateName, $result = [])
    {
        $templateNameExpl   = explode('\\', $templateName);
        $templateNameImpl   = implode('/', $templateNameExpl);
        $this->pathTemplate = ROOT_DIR . '/app/View/' . $templateNameImpl . '.php';
        $this->result       = $result;
    }
    
    /**
     * @return false|string
     */
    public function __invoke()
    {
        ob_start();
        /** @noinspection PhpUnusedLocalVariableInspection */
        $result = $this->result;
        /** @noinspection PhpIncludeInspection */
        require $this->pathTemplate;
        return ob_get_clean();
    }
}
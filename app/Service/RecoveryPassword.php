<?php

namespace Service;

use App\Helper\Request;
use App\Helper\Generator;
use App\Helper\Mail;

class RecoveryPassword
{
    
    public static function addUser($email)
    {
       
        $recoveryToken = Generator::generateToken();
        
        $mail = new Mail();
        $mail->setTo($email);
        $mail->setTheme('Восстановление пароля');
        $message = Request::getDomainUrl() . 'user/recoveryPassword?token=' . $recoveryToken;
        $mail->setMessage($message);
        return $mail->send() ? $recoveryToken : false;
    }
}
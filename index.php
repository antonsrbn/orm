<?php
define('ROOT_DIR', __DIR__);
spl_autoload_register(static function ($className) {
    $classNameExpl = explode('\\', $className);
    $classNameExpl = array_map(static function ($item) {
        return ucfirst($item);
    }, $classNameExpl);
    $classNameImpl = implode('/', $classNameExpl);
    $path          = ROOT_DIR . '/app/' . $classNameImpl . '.php';
    if (file_exists($path)) {
        /** @noinspection PhpIncludeInspection */
        require $path;
    } else {
        die("Класс $path не найден!");
    }
});
require __DIR__ . '/project.php';